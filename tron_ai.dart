import 'dart:io';
import 'dart:convert';
import 'dart:collection';

void main() {
  Game tron = new Game();

  // Read init information from standard input, if any
  stdin.lineMode = true;
  stdin
    .transform(new Utf8Decoder())
    .listen(tron.cycle);
}

class Game {
  Grid grid = new Grid();
  Hero hero = new Hero(new Coordinate(0,0));
  final Coordinate deadCoord = new Coordinate(-1,-1);
  bool needSetup = true;
  Map info;
  List players;

  void cycle(String input) {
    // split the input to lines
    List lines = input.split('\n');

    if (needSetup) { // only need to do this once
      setup(lines.first);
    }

    players.forEach( (player) {
      if( player["alive"] ) { // only move the live players ;)
        var coord = parseLine( lines[player["line"]] );

        if( coord == deadCoord ) { // is he dead?
          player["alive"] = false;
          grid.clearPositions( player["moves"] );
          player["moves"].clear();
        }
        else { // he lives!
          player["moves"].add( coord );
          grid.markPosition( coord );
        }
      }
    } );

    // update our hero position
    hero.updatePosition( parseLine( lines[info["heroIndex"]] ) );

    // calculate cell values for scoring
    grid.updateQuadScores();

    // move our hero
    route();
  }

  void setup(String infoLine) {
    needSetup = false;

    info = {
             "heroIndex": int.parse(infoLine.split(" ").last) + 1,
             "playerCount": int.parse(infoLine.split(" ").first)
           };

    players = new List();

    for( var i = 1; i <= info["playerCount"]; i++ ) {
      players.add({ "line": i, "alive": true, "moves": new Set() });
    }
  }

  Coordinate parseLine(String line) {
    var coord = line.split(" ");
    return new Coordinate.fromList([int.parse(coord[2]), int.parse(coord[3])]);
  }

  void route() {
    var pos = hero.position;
    var recursionLimit = ((1 - grid.vacancyRatio()) * 10).ceil();

//    var stopwatch = new Stopwatch()..start();
//    stderr.writeln("get vacancy ratio (${grid.vacancyRatio()}): ${stopwatch.elapsedMicroseconds}");
//    stopwatch..reset()..start();

    var opts = [
                { "direction": "LEFT",  "score": grid.altRayCast(pos, -1,  0) },
                { "direction": "RIGHT", "score": grid.altRayCast(pos,  1,  0) },
                { "direction": "UP",    "score": grid.altRayCast(pos,  0, -1) },
                { "direction": "DOWN",  "score": grid.altRayCast(pos,  0,  1) }
//                  { "direction": "LEFT",  "score": grid.floodFill( pos.west(), 0, recursionLimit ) },
//                  { "direction": "RIGHT", "score": grid.floodFill( pos.east(), 0, recursionLimit ) },
//                  { "direction": "UP",    "score": grid.floodFill( pos.north(), 0, recursionLimit ) },
//                  { "direction": "DOWN",  "score": grid.floodFill( pos.south(), 0, recursionLimit ) }
              ];

//    stderr.writeln("set opts: ${stopwatch.elapsedMicroseconds}");
//    stopwatch..reset()..start();
    stderr.writeAll(opts, "\n");

    // TODO: Should the current bearing get bonus points?
//    var bearing = opts.singleWhere( (opt) => opt["direction"] == hero.bearing() );
//    bearing['score'] *= 1.1;

    opts.sort( (a, b) => a["score"].compareTo(b["score"]) );
//    stderr.writeln("sort opts: ${stopwatch.elapsedMicroseconds}");
//    stopwatch.stop();

    print( opts.last["direction"] );
  }

}

class Coordinate {
  int x = 0, y = 0;

  Coordinate(this.x, this.y);

  Coordinate.fromList(List input) {
    x = input[0];
    y = input[1];
  }

  Coordinate.fromString(String input) {
    var parts = input.split(',');
    x = int.parse(parts[0]);
    y = int.parse(parts[1]);
  }

  bool operator ==(other) => x == other.x && y == other.y;

  Coordinate north() => new Coordinate(x, y - 1);

  Coordinate south() => new Coordinate(x, y + 1);

  Coordinate west() => new Coordinate(x - 1, y);

  Coordinate east() => new Coordinate(x + 1, y);

  String toString() => "$x,$y";
}

class Hero {
  Coordinate position, lastPosition;

  Hero(Coordinate pos) {
    position = pos;
    lastPosition = pos;
  }

  void updatePosition(Coordinate pos) {
    lastPosition = position;
    position = pos;
  }

  String bearing() {
    if      (position.x < lastPosition.x) { return "LEFT"; }
    else if (position.x > lastPosition.x) { return "RIGHT"; }
    else if (position.y > lastPosition.y) { return "UP"; }
    else if (position.y < lastPosition.y) { return "DOWN"; }
    // let's just pick a direction at random
    else { return "LEFT"; }
  }
}

class Grid {
  int width = 30, height = 20;
  Map<String, bool> cells = new LinkedHashMap();
  Map<String, int> quadScores = new LinkedHashMap();

  Grid() {
    for( var y = 0; y < height; y++ ) {
      for( var x = 0; x < width; x++ ) {
        cells["$x,$y"] = true;
        quadScores["$x,$y"] = 0;
      }
    }
  }

  void markPosition( Coordinate pos ) {
    cells[ pos.toString() ] = false;
  }

  void clearPositions( Set<Coordinate> positions ) {
    positions.forEach( (pos) {
      cells[ pos.toString() ] = true;
    } );
  }

  bool positionAvailable( Coordinate pos ) {
    if ( validatePosition(pos) ) {
      return cells[ pos.toString() ];
    }
    return false;
  }

  bool validatePosition( Coordinate pos ) {
    return pos.x >= 0 && pos.x < width && pos.y >= 0 && pos.y < height;
  }

  num vacancyRatio() => cells.values.where( (value) => value ).length / cells.length;

  void updateQuadScores() {
    cells.forEach( (cellKey, value) {
      var score = 0;
      var cell = new Coordinate.fromString(cellKey);
      score += !validatePosition(cell.north()) || positionAvailable(cell.north()) ? 1 : 0;
      score += !validatePosition(cell.south()) || positionAvailable(cell.south()) ? 1 : 0;
      score += !validatePosition(cell.east())  || positionAvailable(cell.east())  ? 1 : 0;
      score += !validatePosition(cell.west())  || positionAvailable(cell.west())  ? 1 : 0;
      quadScores[cellKey] = score;
    } );
  }

  int altFloodFill( Coordinate pos, [num valUse = 10] ) {
    if( !positionAvailable(pos) ) {
      return 0;
    }

    List<int> xdirs  = [ -1,  0,  1,  0 ];
    List<int> ydirs  = [  0, -1,  0,  1 ];

    var queue = [];
    queue.add( pos );
    var blah = new Map.from(cells);

    blah["$pos"] = valUse;

    var current;
    var tempSpot;
    var score = 1;

    while(queue.length > 0){
      current = queue.removeAt(0);

      for(var i = 0; i < xdirs.length; i++){
        tempSpot = new Coordinate( current.x + xdirs[i], current['y'] + ydirs[i] );

        if( blah["$tempSpot"] ) {
          score++;
          blah["$tempSpot"] = false;
          queue.add(tempSpot);
        }

      }
    }
    return score;
  }

  int floodFill( Coordinate pos, int recursionLevel, int recursionLimit ) {
    List visited = [];
    var score = 0;
    stderr.writeln("floodFill: $pos -- $recursionLevel / $recursionLimit");

    int _flood( Coordinate p ) {
      if ( visited.contains(p) || !positionAvailable(p) || recursionLevel > recursionLimit ) {
        return 0;
      }
      stderr.writeln("\t $p -- $recursionLevel / $recursionLimit");

      visited.add(p);
      score += quadScores["$p"];
      recursionLevel++;

      score += _flood( p.west() );
      score += _flood( p.east() );
      score += _flood( p.north() );
      score += _flood( p.south() );

      return score;
    };

    score += _flood(pos);
    stderr.writeln("\t $visited");
    return score;
  }

  num rayCast( Coordinate pos, num xOff, num yOff ) {
    var i = 1,
        score = 0;

    while( positionAvailable(new Coordinate(pos.x + i * xOff, pos.y + i * yOff)) ) {

      var x = pos.x + i * xOff,
          y = pos.y + i * yOff;

      if( yOff == 0 ) { // casting on the x axis
        var j = 1;
        while( positionAvailable(new Coordinate(x, y + j)) ) { score += 0.25; j++; }
        j = 1;
        while( positionAvailable(new Coordinate(x, y - j)) ) { score += 0.25; j++; }
      }
      else if ( xOff == 0) { // casting on the y axis
        var j = 1;
        while( positionAvailable(new Coordinate(x + j, y)) ) { score += 0.25; j++; }
        j = 1;
        while( positionAvailable(new Coordinate(x - j, y)) ) { score += 0.25; j++; }
      }

      score += 1;
      i++;
    }

    stderr.writeln("rayCast: $pos --> $score");
    return score;
  }

  num altRayCast( Coordinate pos, num xOff, num yOff ) {
    var i = 1,
        score = 0;
    stderr.writeln("rayCast: $pos");

    while( positionAvailable(new Coordinate(pos.x + i * xOff, pos.y + i * yOff)) ) {
      var x = pos.x + i * xOff,
          y = pos.y + i * yOff;

      var j = 1;
      while( positionAvailable(new Coordinate(x, y + j)) ) { score += quadScores["$x,${y + j}"]; j++; }
      j = 1;
      while( positionAvailable(new Coordinate(x, y - j)) ) { score += quadScores["$x,${y - j}"]; j++; }
      j = 1;
      while( positionAvailable(new Coordinate(x + j, y)) ) { score += quadScores["${x + j},$y"]; j++; }
      j = 1;
      while( positionAvailable(new Coordinate(x - j, y)) ) { score += quadScores["${x - j},$y"]; j++; }

      var qs = quadScores["$x,$y"];
      score += qs;
      i++;
      stderr.writeln("\t $x,$y --> $qs");
    }

    stderr.writeln("\t = $score \n");
    return score;
  }

}
